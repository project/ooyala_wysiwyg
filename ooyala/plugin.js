(function ($) {
  CKEDITOR.plugins.add( 'ooyala', {
    init: function( editor ) {
      editor.addCommand( 'ooyala', new CKEDITOR.dialogCommand( 'ooyalaDialog' )),
      editor.ui.addButton( 'ooyala', {
        label: 'Add the Ooyala embed code.', //this is the tooltip text for the button
        command: 'ooyala',
        icon: this.path + 'ooyala.png',
      });
    }
  });
  CKEDITOR.dialog.add( 'ooyalaDialog', function( editor ) {
    return {
      title: 'Embed code for Ooyala video',
      minWidth: 400,
      minHeight: 200,
      contents: [{
        id: 'tab-basic',
        label: 'Embed code',
        elements: [
          {
            type: 'textarea',
            id: 'ooyala',
            label: 'Embed code',
            validate: CKEDITOR.dialog.validate.notEmpty("The embed code field cannot be empty.")
          },
        ]
      }],
      onOk: function() {
        var dialog = this;
        var embed_value = dialog.getValueOf('tab-basic', 'ooyala');
        if (embed_value.indexOf('OO.Player.create') >= 0 && embed_value.indexOf('iframe.js') < 0) {
          embed_value = embed_value.replace(/\r\n/g, '').replace(/[\r\n]/g, '');
          var str = $(embed_value);
          var src = str[0].src;
          var indexofsrc = src.indexOf('v3/');
          var src = src.substring(indexofsrc + 3);
          var stylestring = $(str[1]).attr('style');
          var style = stylestring.replace('width:', '').replace(';height', '');
          var indexofpid = str[2].text.indexOf("player', '");
          var indexofpide = str[2].text.indexOf("'); });");
          var pbid = str[2].text.substring(indexofpid + 10, indexofpide);
          editor.insertHtml("{ooyala:3:" + src + ":" + pbid + ":" + style + "}");
        }
        else if (embed_value.indexOf('OO.Player.create') < 0 && embed_value.indexOf('iframe.js') >= 0) {
          var $str = $(embed_value);
          var src = $str.attr('src');
          var height = parseInt($str.attr('height'), 10);
          var width = parseInt($str.attr('width'), 10);
          var indexofpbid = src.indexOf('pbid=');
          var indexofec = src.indexOf('ec=');
          var pbid, ec = 0;
          if (indexofec > indexofpbid) {
            pbid = src.substring(indexofpbid + 5, indexofec - 1);
            ec = src.substring(indexofec + 3);
          }
          else {
            pbid = src.substring(indexofpbid + 5);
            ec = src.substring(indexofec + 3, indexofpbid - 1);
          }
          // Insert the embed code for the version 2 embed code.
          editor.insertHtml("{ooyala:2:" + pbid + ":" + ec + ":" + height + ":" + width + "}");
        }
      }
    };
  });
})(jQuery);
